import { BrowserRouter, Routes, Route } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import store, { persistor } from "./redux/store";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Profile from "./pages/profile/Profile";
import Register from "./pages/register/Register";
import "./global.css";
import ProtectedRoute from "./components/ProtectedRoute";

const queryClient = new QueryClient({
	defaultOptions: {
		queries: {
			retry: false,
		},
	},
});

function App() {
	return (
		<QueryClientProvider client={queryClient}>
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<BrowserRouter>
						<Routes>
							<Route element={<ProtectedRoute />}>
								<Route path="/" element={<Home />} />
								<Route
									path="profile/:id"
									element={<Profile />}
								></Route>
							</Route>
							<Route path="/" element={<Home />}></Route>
							<Route path="login" element={<Login />}></Route>
							<Route
								path="register"
								element={<Register />}
							></Route>
							<Route
								path="*"
								element={<h1>Page not found!</h1>}
							></Route>
						</Routes>
					</BrowserRouter>
				</PersistGate>
			</Provider>
		</QueryClientProvider>
	);
}

export default App;
