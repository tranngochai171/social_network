import {
	Box,
	Button,
	InputBase,
	Stack,
	styled,
	Typography,
} from "@mui/material";

const LoginContainer = styled(Box)({
	width: "100vw",
	height: "100vh",
	backgroundColor: "#f0f2f5",
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
});

const LoginWrapper = styled(Box)(({ theme }) => ({
	width: "70%",
	height: "70%",
	display: "flex",
	alignItems: "center",
	[theme.breakpoints.down("md")]: {
		flexDirection: "column",
	},
}));

const LoginSide = styled(Box)({
	width: "100%",
	flex: 1,
});

const LoginBox = styled(Stack)({
	backgroundColor: "#fff",
	// height: "300px",
	borderRadius: "10px",
	padding: "20px",
	justifyContent: "space-between",
});

const StyledInput = styled(InputBase)({
	border: "1px solid gray",
	padding: "10px",
	borderRadius: "10px",
});

const LoginButton = styled(Button)({
	backgroundColor: "#1776ee",
	borderRadius: "10px",
	height: "50px",
	color: "#fff",
	":hover": {
		color: "#fff",
		backgroundColor: "#1776ee",
	},
});

const LoginAccount = styled(Button)(({ theme }) => ({
	height: "50px",
	width: "60%",
	alignSelf: "center",
	backgroundColor: "#42b72a",
	borderRadius: "10px",
	color: "#fff",
	":hover": {
		color: "#fff",
		backgroundColor: "#42b72a",
	},
	[theme.breakpoints.down("md")]: {
		width: "100%",
	},
}));

const Login = () => {
	return (
		<LoginContainer>
			<LoginWrapper>
				<LoginSide>
					<Stack gap="1">
						<Typography
							variant="h3"
							sx={{ color: "#1755ee" }}
							fontWeight="800"
						>
							TopyNetwork
						</Typography>
						<Typography variant="h6">
							Connect with friends and the world around you on
							TopyNetwork
						</Typography>
					</Stack>
				</LoginSide>
				<LoginSide sx={{ maxWidth: "500px" }}>
					<LoginBox gap={3}>
						<StyledInput placeholder="User name" />
						<StyledInput placeholder="Email" />
						<StyledInput placeholder="Password" />
						<LoginButton>Sign up</LoginButton>
						<LoginAccount>Log into Account</LoginAccount>
					</LoginBox>
				</LoginSide>
			</LoginWrapper>
		</LoginContainer>
	);
};

export default Login;
