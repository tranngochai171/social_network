import {
	Box,
	Button,
	InputBase,
	Stack,
	styled,
	Typography,
} from "@mui/material";
import { Formik, Form } from "formik";
import * as yup from "yup";
import { Link, useLocation, Navigate } from "react-router-dom";
import FormikControl, {
	CONTROL_TYPE,
} from "../../components/formik/formik-control";
import { useLogin } from "../../hooks/useAuthentication";
import { useSelector } from "react-redux";
import { getIsAuthenticated } from "../../redux/selectors/auth";

const validationSchema = yup.object({
	email: yup
		.string()
		.email("Not a valid email address")
		.required("Email is required"),
	password: yup.string().required("Password is required"),
});

const initialValues = {
	email: "",
	password: "",
};

const LoginContainer = styled(Box)({
	width: "100vw",
	height: "100vh",
	backgroundColor: "#f0f2f5",
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
});

const LoginWrapper = styled(Box)(({ theme }) => ({
	width: "70%",
	height: "70%",
	display: "flex",
	alignItems: "center",
	[theme.breakpoints.down("md")]: {
		flexDirection: "column",
	},
}));

const LoginSide = styled(Box)({
	width: "100%",
	flex: 1,
});

const LoginBox = styled(Stack)({
	backgroundColor: "#fff",
	// height: "300px",
	borderRadius: "10px",
	padding: "20px",
	justifyContent: "space-between",
});

const StyledInput = styled(InputBase)({
	border: "1px solid gray",
	padding: "10px",
	borderRadius: "10px",
});

const LoginButton = styled(Button)({
	backgroundColor: "#1776ee",
	borderRadius: "10px",
	height: "50px",
	color: "#fff",
	":hover": {
		color: "#fff",
		backgroundColor: "#1776ee",
	},
	"&.Mui-disabled": {
		color: "#fff",
	},
});

const RegisterButton = styled(Button)(({ theme }) => ({
	height: "50px",
	width: "60%",
	alignSelf: "center",
	backgroundColor: "#42b72a",
	borderRadius: "10px",
	color: "#fff",
	":hover": {
		color: "#fff",
		backgroundColor: "#42b72a",
	},
	[theme.breakpoints.down("md")]: {
		width: "100%",
	},
}));

const Login = () => {
	const { mutate } = useLogin();
	const onSubmit = (value) => {
		mutate({ email: value.email, pwd: value.password });
	};
	const isAuthenticated = useSelector(getIsAuthenticated);
	const location = useLocation();
	const redirectUrl = location.state?.from?.pathname || "/";
	const formikProps = { initialValues, validationSchema, onSubmit };
	if (isAuthenticated) {
		return <Navigate to={redirectUrl} />;
	}
	return (
		<Formik {...formikProps}>
			{({ isValid, handleSubmit }) => (
				<Form>
					<LoginContainer>
						<LoginWrapper>
							<LoginSide>
								<Stack gap="1">
									<Typography
										variant="h3"
										sx={{ color: "#1755ee" }}
										fontWeight="800"
									>
										TopyNetwork
									</Typography>
									<Typography variant="h6">
										Connect with friends and the world
										around you on TopyNetwork
									</Typography>
								</Stack>
							</LoginSide>
							<LoginSide sx={{ maxWidth: "500px" }}>
								<LoginBox gap={3}>
									<FormikControl
										control={CONTROL_TYPE.INPUT}
										name="email"
										label="Email"
										required
									/>
									<FormikControl
										control={CONTROL_TYPE.INPUT}
										name="password"
										label="Password"
										required
									/>

									<LoginButton
										disabled={!isValid}
										onClick={handleSubmit}
									>
										Log in
									</LoginButton>
									<Typography
										sx={{
											alignSelf: "center",
											color: "#1755ee",
										}}
										variant="body1"
										fontWeight="400"
									>
										Forgot Password?
									</Typography>
									<RegisterButton>
										<Link
											style={{
												color: "#fff",
												textDecoration: "none",
											}}
											to="/register"
										>
											Create a New Account
										</Link>
									</RegisterButton>
								</LoginBox>
							</LoginSide>
						</LoginWrapper>
					</LoginContainer>
				</Form>
			)}
		</Formik>
	);
};

export default Login;
