import { Stack, Box, styled, Typography } from "@mui/material";
import Feed from "../../components/Feed";
import Navbar from "../../components/Navbar";
import Rightbar from "../../components/Rightbar";
import Sidebar from "../../components/Sidebar";

const CoverContainer = styled(Box)({
	height: "320px",
	width: "100%",
	position: "relative",
});

const CoverPicture = styled("img")({
	width: "100%",
	height: "250px",
	objectFit: "cover",
});

const AvatarPicture = styled("img")({
	width: "150px",
	height: "150px",
	position: "absolute",
	borderRadius: "50%",
	objectFit: "cover",
	border: "5px solid #fff",
	top: "50%",
	left: 0,
	right: 0,
	margin: "auto",
});

const PROFILE_FOLDER = process.env.REACT_APP_PUBLIC_FOLDER;

const Profile = () => {
	return (
		<>
			<Navbar />
			<Stack direction="row" gap={1}>
				<Sidebar />
				<Stack flex="9" direction="column" gap={2}>
					<CoverContainer sx={{}}>
						<CoverPicture src={`${PROFILE_FOLDER}/post/1.jpeg`} />
						<AvatarPicture
							src={`${PROFILE_FOLDER}/person/4.jpeg`}
						/>
					</CoverContainer>
					<Stack gap={1} sx={{ alignItems: "center" }}>
						<Typography variant="h4" fontWeight="bold">
							Topy
						</Typography>
						<Typography variant="subtitle1">
							Hello there!
						</Typography>
					</Stack>
					<Stack direction="row">
						<Feed />
						<Rightbar isProfilePage />
					</Stack>
				</Stack>
			</Stack>
		</>
	);
};

export default Profile;
