import { Stack } from "@mui/material";
import Feed from "../../components/Feed";
import Navbar from "../../components/Navbar";
import Rightbar from "../../components/Rightbar";
import Sidebar from "../../components/Sidebar";

const Home = () => {
	return (
		<>
			<Navbar />
			<Stack direction="row" gap={1}>
				<Sidebar />
				<Feed />
				<Rightbar />
			</Stack>
		</>
	);
};

export default Home;
