import { Box, styled, Stack } from "@mui/material";
import Post from "./Post";
import Share from "./Share";
import { Posts } from "../dummyData";

const FeedContainer = styled(Box)({
	flex: 5.5,
});

const FeedWrapper = styled(Stack)({
	padding: "20px",
});

const Feed = () => {
	return (
		<FeedContainer>
			<FeedWrapper gap={5}>
				<Share />
				{Posts.map((post) => (
					<Post key={post.id} post={post} />
				))}
			</FeedWrapper>
		</FeedContainer>
	);
};

export default Feed;
