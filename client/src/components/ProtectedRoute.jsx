import { Navigate, Outlet, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { getIsAuthenticated } from "../redux/selectors/auth";

const ProtectedRoute = ({ redirectPath = "/login", children }) => {
	const isAuthenticated = useSelector(getIsAuthenticated);
	const location = useLocation();
	if (!isAuthenticated) {
		return (
			<Navigate to={redirectPath} replace state={{ from: location }} />
		);
	}

	return children ? children : <Outlet />;
};

export default ProtectedRoute;
