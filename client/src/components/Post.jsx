import {
	Paper,
	Stack,
	styled,
	IconButton,
	Avatar,
	Typography,
	Box,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { Users } from "../dummyData";

const PostContainer = styled(Paper)({
	padding: "20px",
});

const HeaderPost = styled(Stack)({
	alignItems: "center",
	justifyContent: "space-between",
	marginBottom: "20px",
});

const BodyPost = styled(Box)({
	marginBottom: "20px",
});

const PostImage = styled("img")({
	width: "100%",
	maxHeight: "500px",
	objectFit: "contain",
});

const BottomPost = styled(Box)({});

const ReactIcon = styled("img")({
	width: "24px",
	height: "24px",
	cursor: "pointer",
});

const findUser = (userId) => {
	const user = Users.find((user) => user.id === userId);
	return { username: user.username, picture: user.profilePicture };
};

const PUBLIC_FOLDER = process.env.REACT_APP_PUBLIC_FOLDER;

const Post = ({ post }) => {
	const { desc, date, photo, like, comment, userId } = post;

	const { username, picture } = findUser(userId);
	return (
		<PostContainer elevation={4}>
			<HeaderPost direction="row">
				<Stack direction="row" gap={2} sx={{ alignItems: "center" }}>
					<Avatar src={`${PUBLIC_FOLDER}/${picture}`} />
					<Typography variant="h6" fontWeight="800">
						{username}
					</Typography>
					<Typography variant="subtitle1">{date}</Typography>
				</Stack>
				<IconButton>
					<MoreVertIcon />
				</IconButton>
			</HeaderPost>
			<BodyPost>
				<Stack gap={1}>
					<Typography variant="body1">{desc}</Typography>
					<PostImage src={`${PUBLIC_FOLDER}/${photo}`} />
				</Stack>
			</BodyPost>
			<BottomPost>
				<Stack
					direction="row"
					sx={{
						alignItems: "center",
						justifyContent: "space-between",
					}}
				>
					<Stack direction="row" gap={1}>
						<ReactIcon src={`${PUBLIC_FOLDER}/like.png`} />
						<ReactIcon src={`${PUBLIC_FOLDER}/heart.png`} />
						<Typography>{like} people like it</Typography>
					</Stack>
					<Typography>{comment} comments</Typography>
				</Stack>
			</BottomPost>
		</PostContainer>
	);
};

export default Post;
