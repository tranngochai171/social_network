import {
	Stack,
	styled,
	Avatar,
	InputBase,
	Paper,
	Divider,
	Button,
} from "@mui/material";
import PhotoLibraryIcon from "@mui/icons-material/PhotoLibrary";
import LabelIcon from "@mui/icons-material/Label";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EmojiEmotionsIcon from "@mui/icons-material/EmojiEmotions";

const ShareContainer = styled(Paper)({
	padding: "20px",
});

const Share = () => {
	return (
		<ShareContainer elevation={4}>
			<Stack direction="row" gap={2}>
				<Avatar sx={{ width: 56, height: 56 }}>T</Avatar>
				<InputBase placeholder="What's in your mind?" fullWidth />
			</Stack>
			<Divider sx={{ margin: "20px 0" }} />
			<Stack direction="row" sx={{ justifyContent: "space-between" }}>
				<Stack direction="row" gap={2}>
					<Button startIcon={<PhotoLibraryIcon />}>
						Photo or Video
					</Button>
					<Button startIcon={<LabelIcon />}>Tag</Button>
					<Button startIcon={<LocationOnIcon />}>Location</Button>
					<Button startIcon={<EmojiEmotionsIcon />}>Feelings</Button>
				</Stack>
				<Button variant="contained" sx={{ marginRight: "20px" }}>
					Share
				</Button>
			</Stack>
		</ShareContainer>
	);
};

export default Share;
