import {
	Box,
	styled,
	Stack,
	Typography,
	ListItem,
	ListItemButton,
	ListItemAvatar,
	Avatar,
	ListItemText,
	Badge,
	List,
} from "@mui/material";
import { Users } from "../dummyData";

const RightbarContainer = styled(Box)({
	flex: 3.5,
});

const RightbarWrapper = styled(Stack)({
	padding: "20px",
});

const BirthdayContainer = styled(Stack)({
	alignItems: "center",
});

const BirthdayIcon = styled("img")({
	width: "50px",
	height: "50px",
});

const AdvertiseImg = styled("img")({
	borderRadius: "10px",
	width: "100%",
});

const OnlineBadge = styled(Badge)(({ theme }) => ({
	"& .MuiBadge-badge": {
		backgroundColor: "#44b700",
		color: "#44b700",
		boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
		"&::after": {
			position: "absolute",
			top: -1.3,
			left: -1.1,
			width: "100%",
			height: "100%",
			borderRadius: "50%",
			animation: "ripple 1.2s infinite ease-in-out",
			border: "1px solid currentColor",
			content: '""',
		},
	},
	"@keyframes ripple": {
		"0%": {
			transform: "scale(.8)",
			opacity: 1,
		},
		"100%": {
			transform: "scale(2.4)",
			opacity: 0,
		},
	},
}));

const UserInfo = styled(Stack)({});

const UserFriends = styled(Stack)({});

const FriendStack = styled(Stack)({
	flexWrap: "wrap",
});

const FriendImage = styled("img")({
	width: "100px",
	height: "100px",
	borderRadius: "10px",
	objectFit: "cover",
});

const PUBLIC_FOLDER = process.env.REACT_APP_PUBLIC_FOLDER;

const Rightbar = ({ isProfilePage }) => {
	const HomePageRightbar = () => (
		<>
			<BirthdayContainer direction="row" gap={1}>
				<BirthdayIcon src={`${PUBLIC_FOLDER}/gift.png`} />
				<Typography>
					<b>John</b> and <b>3 other friends</b> have a birthday
					today.
				</Typography>
			</BirthdayContainer>
			<AdvertiseImg src={`${PUBLIC_FOLDER}/ad.png`} />
			<Stack direction="column" gap={2}>
				<Typography variant="h6">Online Friends</Typography>
				<List>
					{Users.map((user) => (
						<ListItem key={user.id} disablePadding>
							<ListItemButton sx={{ borderRadius: "10px" }}>
								<ListItemAvatar>
									<OnlineBadge
										overlap="circular"
										anchorOrigin={{
											vertical: "bottom",
											horizontal: "right",
										}}
										variant="dot"
									>
										<Avatar
											src={`${PUBLIC_FOLDER}/${user.profilePicture}`}
										/>
									</OnlineBadge>
								</ListItemAvatar>
								<ListItemText
									primary={
										<Typography
											variant="body1"
											fontWeight="800"
										>
											{user.username}
										</Typography>
									}
								/>
							</ListItemButton>
						</ListItem>
					))}
				</List>
			</Stack>
		</>
	);
	const ProfileRightbar = () => (
		<Stack gap={2}>
			<UserInfo gap={1}>
				<Typography variant="h6">User information</Typography>
				<Stack direction="row" gap={1}>
					<Typography variant="subtitle1" fontWeight="800">
						City:
					</Typography>
					<Typography variant="subtitle1" fontWeight="300">
						New York
					</Typography>
				</Stack>
				<Stack direction="row" gap={1}>
					<Typography variant="subtitle1" fontWeight="800">
						From:
					</Typography>
					<Typography variant="subtitle1" fontWeight="300">
						Madrid
					</Typography>
				</Stack>
				<Stack direction="row" gap={1}>
					<Typography variant="subtitle1" fontWeight="800">
						Relationship:
					</Typography>
					<Typography variant="subtitle1" fontWeight="300">
						Single
					</Typography>
				</Stack>
			</UserInfo>
			<UserFriends gap={1}>
				<Typography variant="h6">User Friends</Typography>
				<FriendStack direction="row" gap={2}>
					{Users.map((user) => (
						<Stack key={user.id}>
							<FriendImage
								src={`${PUBLIC_FOLDER}/${user.profilePicture}`}
							/>
							<Typography variant="subtitle1">
								John Carter
							</Typography>
						</Stack>
					))}
				</FriendStack>
			</UserFriends>
		</Stack>
	);
	return (
		<RightbarContainer>
			<RightbarWrapper gap={3}>
				{isProfilePage ? <ProfileRightbar /> : <HomePageRightbar />}
			</RightbarWrapper>
		</RightbarContainer>
	);
};

export default Rightbar;
