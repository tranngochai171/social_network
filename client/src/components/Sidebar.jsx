import {
	Box,
	List,
	ListItem,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Divider,
	styled,
	ListItemAvatar,
	Avatar,
} from "@mui/material";
import { Users } from "../dummyData";
import RssFeedIcon from "@mui/icons-material/RssFeed";
import ChatIcon from "@mui/icons-material/Chat";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import GroupIcon from "@mui/icons-material/Group";
import BookmarksIcon from "@mui/icons-material/Bookmarks";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import WorkIcon from "@mui/icons-material/Work";
import EventIcon from "@mui/icons-material/Event";
import SchoolIcon from "@mui/icons-material/School";

const SidebarContainer = styled(Box)({
	flex: 3,
	height: "calc(100vh - 64px)",
	overflowY: "scroll",
	position: "sticky",
	top: "64px",
});

const SidebarWrapper = styled(Box)({
	padding: 2.5,
});

const PUBLIC_FOLDER = process.env.REACT_APP_PUBLIC_FOLDER;

const Sidebar = () => {
	return (
		<SidebarContainer flex="3">
			<SidebarWrapper>
				<List>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<RssFeedIcon />
							</ListItemIcon>
							<ListItemText primary="Feed" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<ChatIcon />
							</ListItemIcon>
							<ListItemText primary="Chats" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<PlayCircleIcon />
							</ListItemIcon>
							<ListItemText primary="Videos" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<GroupIcon />
							</ListItemIcon>
							<ListItemText primary="Groups" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<BookmarksIcon />
							</ListItemIcon>
							<ListItemText primary="Bookmarks" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<HelpOutlineIcon />
							</ListItemIcon>
							<ListItemText primary="Questions" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<WorkIcon />
							</ListItemIcon>
							<ListItemText primary="Jobs" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<EventIcon />
							</ListItemIcon>
							<ListItemText primary="Events" />
						</ListItemButton>
					</ListItem>
					<ListItem disablePadding>
						<ListItemButton>
							<ListItemIcon>
								<SchoolIcon />
							</ListItemIcon>
							<ListItemText primary="Courses" />
						</ListItemButton>
					</ListItem>
				</List>
				<Divider />
				<List>
					{Users.map((user) => (
						<ListItem key={user.id} disablePadding>
							<ListItemButton>
								<ListItemAvatar>
									<Avatar
										src={`${PUBLIC_FOLDER}/${user.profilePicture}`}
									/>
								</ListItemAvatar>
								<ListItemText primary={user.username} />
							</ListItemButton>
						</ListItem>
					))}
				</List>
			</SidebarWrapper>
		</SidebarContainer>
	);
};

export default Sidebar;
