import {
	Box,
	styled,
	Stack,
	TextField,
	Button,
	Alert,
	Container,
} from "@mui/material";
import { useState, useEffect } from "react";
import useAuth from "../hooks/useAuth";

const CustomBox = styled(Box)({
	height: "100vh",
	display: "flex",
	alignItems: "center",
	justifyContent: "center",
});

const Login = ({ setUser }) => {
	const { mutate, error, data } = useAuth();
	const [userName, setUserName] = useState("");
	const [password, setPassword] = useState("");
	useEffect(() => {
		if (data) {
			setUser(data.data);
		}
	}, [data, setUser]);
	const handleChangeInput = (e, key) => {
		switch (key) {
			case "userName":
				setUserName(e.target.value);
				break;
			case "password":
				setPassword(e.target.value);
				break;
			default:
		}
	};
	const handleSubmit = (_e) => {
		mutate({ userName, pwd: password });
	};
	return (
		<Container>
			<CustomBox>
				<Stack direction="column" gap={2}>
					{error && (
						<Alert severity="error">
							{error?.response?.data?.message}
						</Alert>
					)}
					<TextField
						id="filled-basic"
						label="User name"
						variant="filled"
						value={userName}
						onChange={(e) => handleChangeInput(e, "userName")}
					/>
					<TextField
						id="filled-basic"
						label="Password"
						variant="filled"
						value={password}
						onChange={(e) => handleChangeInput(e, "password")}
					/>
					<Button variant="contained" onClick={handleSubmit}>
						Login
					</Button>
				</Stack>
			</CustomBox>
		</Container>
	);
};

export default Login;
