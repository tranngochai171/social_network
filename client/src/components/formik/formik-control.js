import Input from "./input.js";

export const CONTROL_TYPE = {
	INPUT: "input",
};

const FormikControl = (props) => {
	const { control, ...rest } = props;
	switch (control) {
		case CONTROL_TYPE.INPUT:
			return <Input {...rest} />;
		default:
			return null;
	}
};

export default FormikControl;
