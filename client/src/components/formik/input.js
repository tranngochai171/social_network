import { Field } from "formik";
import { TextField } from "@mui/material";
const Input = (props) => {
	const { name, label, ...rest } = props;
	return (
		<Field name={name}>
			{({ field, form, meta }) => {
				return (
					<TextField
						fullWidth
						{...field}
						{...rest}
						label={label}
						inputProps={{
							onBlur: form.handleBlur,
						}}
						error={meta.touched && Boolean(meta.error)}
						helperText={meta.touched && meta.error}
					/>
				);
			}}
		</Field>
	);
};

export default Input;
