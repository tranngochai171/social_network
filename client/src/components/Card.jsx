import {
	Avatar,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	CardMedia,
	IconButton,
	Typography,
} from "@mui/material";
import { useState } from "react";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";

const MyCard = ({ post }) => {
	const { image, name } = post;
	const firstLetter = name[0];
	const [liked, setLiked] = useState(false);

	const handleNotification = () => {
		setLiked(!liked);
	};
	return (
		<Card sx={{ maxWidth: 345 }}>
			<CardHeader
				avatar={
					<Avatar sx={{ bgcolor: "red" }} aria-label="recipe">
						{firstLetter}
					</Avatar>
				}
				action={<IconButton aria-label="settings"></IconButton>}
				title={name}
			/>
			<CardMedia
				component="img"
				height="194"
				image={image}
				alt="Paella dish"
			/>
			<CardContent>
				<Typography variant="body2" color="text.secondary">
					This impressive paella is a perfect party dish and a fun
					meal to cook together with your guests. Add 1 cup of frozen
					peas along with the mussels, if you like.
				</Typography>
			</CardContent>
			<CardActions disableSpacing>
				<IconButton aria-label="add to favorites">
					{liked ? (
						<FavoriteIcon onClick={handleNotification} />
					) : (
						<FavoriteBorderIcon onClick={handleNotification} />
					)}
				</IconButton>
				<IconButton aria-label="share">
					<ShareIcon />
				</IconButton>
			</CardActions>
		</Card>
	);
};

export default MyCard;
