import {
	AppBar,
	Badge,
	Stack,
	styled,
	Toolbar,
	Typography,
	IconButton,
	Box,
	InputBase,
	Avatar,
	Button,
	Menu,
	MenuItem,
} from "@mui/material";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { logout } from "../redux/slices/authSlice";
import SearchIcon from "@mui/icons-material/Search";
import NotificationsIcon from "@mui/icons-material/Notifications";
import PersonIcon from "@mui/icons-material/Person";
import MessageIcon from "@mui/icons-material/Message";

const StyledToolbar = styled(Toolbar)({
	display: "flex",
	alignItems: "center",
});

const NavbarLeft = styled(Box)({
	flex: 3,
});

const Search = styled(Box)(({ theme }) => ({
	backgroundColor: "#fff",
	display: "flex",
	flex: 5,
	borderRadius: theme.shape.borderRadius,
	height: "30px",
	alignItems: "center",
}));

const NavbarRight = styled(Box)(({ color }) => ({
	display: "flex",
	alignItems: "center",
	flex: 4,
	justifyContent: "space-around",
}));

const Navbar = () => {
	const [anchorEl, setAnchorEl] = useState(null);
	const dispatch = useDispatch();
	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};
	return (
		<AppBar position="sticky">
			<StyledToolbar>
				<NavbarLeft flex="3">
					<Typography variant="h4">
						<Link
							to="/"
							style={{ textDecoration: "none", color: "#fff" }}
						>
							TopyNetwork
						</Link>
					</Typography>
				</NavbarLeft>
				<Search>
					<SearchIcon
						sx={{
							fontSize: "24px",
							color: "#000",
							marginLeft: "10px",
							marginRight: "10px",
						}}
					/>
					<InputBase placeholder="Search..." fullWidth />
				</Search>
				<NavbarRight>
					<Stack direction="row" gap={1}>
						<Button disableElevation variant="contained">
							Home page
						</Button>
						<Button disableElevation variant="contained">
							Timeline
						</Button>
					</Stack>
					<Stack direction="row" gap={1}>
						<IconButton>
							<Badge badgeContent={4} color="error">
								<PersonIcon
									sx={{ color: "#fff", fontSize: "24px" }}
								/>
							</Badge>
						</IconButton>
						<IconButton>
							<Badge badgeContent={4} color="error">
								<MessageIcon
									sx={{ color: "#fff", fontSize: "24px" }}
								/>
							</Badge>
						</IconButton>
						<IconButton>
							<Badge badgeContent={4} color="error">
								<NotificationsIcon
									sx={{ color: "#fff", fontSize: "24px" }}
								/>
							</Badge>
						</IconButton>
					</Stack>
					<IconButton onClick={handleClick}>
						<Avatar>T</Avatar>
					</IconButton>
					<Menu
						id="basic-menu"
						anchorEl={anchorEl}
						open={!!anchorEl}
						onClose={() => setAnchorEl(null)}
						MenuListProps={{
							"aria-labelledby": "basic-button",
						}}
						anchorOrigin={{
							vertical: "bottom",
							horizontal: "right",
						}}
						transformOrigin={{
							vertical: "top",
							horizontal: "right",
						}}
					>
						<MenuItem onClick={() => dispatch(logout())}>
							Logout
						</MenuItem>
					</Menu>
				</NavbarRight>
			</StyledToolbar>
		</AppBar>
	);
};

export default Navbar;
