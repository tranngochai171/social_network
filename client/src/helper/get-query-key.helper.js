const USER_TIMELINE_POSTS = "USER_TIMELINE_POSTS";
const TIMELINE_POSTS = "TIMELINE_POSTS";

const getQueryKey = {
	getUserTimelinePosts: (userId) => [USER_TIMELINE_POSTS, userId],
	getTimelinePosts: (userId) => [TIMELINE_POSTS, userId],
};

export default getQueryKey;
