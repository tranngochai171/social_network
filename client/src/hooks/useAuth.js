import { useMutation } from "react-query";
import axios from "axios";

const useAuth = () => {
	return useMutation((payload) =>
		axios.post("http://localhost:3000/auth", payload)
	);
};

export default useAuth;
