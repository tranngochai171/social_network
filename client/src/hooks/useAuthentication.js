import { useMutation } from "react-query";
import { useDispatch } from "react-redux";
import { login } from "../redux/slices/authSlice";
import useAxios from "./useAxios";

export const useLogin = () => {
	const myAxios = useAxios();
	const dispatch = useDispatch();
	return useMutation((payload) => myAxios.post("/auth/login", payload), {
		onSuccess: (response) => {
			if (response.status === 200) {
				dispatch(login(response.data));
			}
		},
	});
};
