import { useEffect } from "react";
import axios from "../helper/axios.helper";

const useAxios = () => {
	useEffect(() => {
		const responseInterceptor = axios.interceptors.response.use(
			(response) => response,
			(error) => {
				const errorMessage = error?.response?.data ?? {
					message: "Something went wrong.",
				};
				return Promise.reject(errorMessage);
			}
		);
		return () => {
			axios.interceptors.response.eject(responseInterceptor);
		};
	}, []);

	return axios;
};

export default useAxios;
