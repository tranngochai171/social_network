require("dotenv").config();
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const connectDB = require("./config/dbConn");
const helmet = require("helmet");
const morgan = require("morgan");
const socketServices = require("./services/socket.service");
const errorHandler = require("./middleware/errorHandler");
const http = require("http");
const app = express();
const server = http.createServer(app);
const socket = require("socket.io");
const io = socket(server);
global.__io = io;
const PORT = process.env.PORT || 8800;

connectDB();

// Cross Origin Resource Sharing
app.use(cors());

// built-in middleware to handle urlencoded form data
app.use(express.urlencoded({ extended: false }));

// built-in middleware for json
app.use(express.json());
app.use(helmet());
app.use(morgan("combined"));

app.use("/auth", require("./routes/api/auth.route"));
app.use("/users", require("./routes/api/users.route"));
app.use("/posts", require("./routes/api/posts.route"));

// app.use('/')
app.all("*", (req, res) => {
	res.status(404).json({ message: "404 Not Found" });
});

// Handle error
app.use(errorHandler);

global.__io.on("connection", socketServices.connection);

mongoose.connection.once("open", () => {
	console.log("Connected to MongoDB");
	// global._io.on("connection", socketServices.connection);
	var server = app.listen(PORT, () => {
		console.log(`Server running on port: ${PORT}`);
	});
});
