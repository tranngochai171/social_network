const bcrypt = require("bcrypt");
const User = require("../models/User");

const updateUser = async (req, res) => {
	try {
		const { id } = req.params;
		if (!id) {
			return res.status(400).json({ message: "UserID is required." });
		}
		if (id === req.body.userId || req.body.isAdmin) {
			if (req.body.password) {
				const salt = await bcrypt.genSalt(10);
				req.body.password = await bcrypt.hash(req.body.password, salt);
			}
			const user = await User.findByIdAndUpdate(
				id,
				{
					$set: req.body,
				},
				{ new: true }
			);
			res.status(200).json({
				user,
				message: "Account has been updated.",
			});
		} else {
			res.status(403).json({
				message: "You can only update your account.",
			});
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

const deleteUser = async (req, res) => {
	try {
		const { id } = req.params;
		if (!id) {
			return res.status(400).json({ message: "UserID is required." });
		}
		if (id === req.body.userId || req.body.isAdmin) {
			// await User.findByIdAndRemove(id);
			await User.deleteOne({ _id: id });
			res.status(200).json({
				message: "Account has been removed.",
			});
		} else {
			res.status(403).json({
				message: "You can only delete your account.",
			});
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

const getUserById = async (req, res) => {
	try {
		const { id } = req.params;
		if (!id) {
			return res.status(400).json({ message: "UserID is required." });
		}
		// const user = await User.findOne({ _id: id });
		const user = await User.findById(id);
		if (!user)
			return res
				.status(404)
				.json({ message: `User not found with ID: ${id}` });
		const { password, updatedAt, ...userWithoutPassword } = user._doc;
		res.status(200).json(userWithoutPassword);
	} catch (err) {
		res.status(500).json(err);
	}
};

const followUser = async (req, res) => {
	try {
		const { id } = req.params;
		if (!id) {
			return res.status(400).json({ message: "UserID is required." });
		}
		if (id !== req.body.userId) {
			const user = await User.findById(id);
			const currentUser = await User.findById(req.body.userId);
			if (!user.followers.includes(req.body.userId)) {
				await user.updateOne({ $push: { followers: req.body.userId } });
				await currentUser.updateOne({ $push: { followings: id } });
				res.status(200).json({ message: "User has been followed." });
			} else {
				res.status(403).json({
					message: "You already follow this user.",
				});
			}
		} else {
			res.status(403).json({
				message: "You can not follow yourself.",
			});
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

const unfollowUser = async (req, res) => {
	try {
		const { id } = req.params;
		if (!id) {
			return res.status(400).json({ message: "UserID is required." });
		}
		if (id !== req.body.userId) {
			const user = await User.findById(id);
			const currentUser = await User.findById(req.body.userId);
			if (user.followers.includes(req.body.userId)) {
				await user.updateOne({ $pull: { followers: req.body.userId } });
				await currentUser.updateOne({ $pull: { followings: id } });
				res.status(200).json({ message: "User has been unfollowed." });
			} else {
				res.status(403).json({
					message: "You already unfollow this user.",
				});
			}
		} else {
			res.status(403).json({
				message: "You can not unfollow yourself.",
			});
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

module.exports = {
	updateUser,
	deleteUser,
	getUserById,
	followUser,
	unfollowUser,
};
