const Post = require("../models/Post");
const User = require("../models/User");

const createPost = async (req, res) => {
	try {
		const newPost = new Post(req.body);
		const savedPost = await newPost.save();
		res.status(200).json(savedPost);
	} catch (err) {
		res.status(500).json(err);
	}
};

const getPostById = async (req, res) => {
	try {
		const post = await Post.findById(req.params.id);
		if (!post) return res.status(404).json({ message: "Post not found." });
		res.status(200).json(post);
	} catch (err) {
		res.status(500).json(err);
	}
};

const updatePost = async (req, res) => {
	try {
		let post;
		try {
			post = await Post.findById(req.params.id);
		} catch (err) {
			return res.status(404).json({
				message: `Unable to find the post with ID: ${req.params.id}`,
			});
		}
		if (post.userId === req.body.userId) {
			await Post.updateOne({ $set: req.body });
			res.status(200).json({ message: "The post has been updated." });
		} else {
			res.status(403).json({ message: "You can only update your post." });
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

const likePost = async (req, res) => {
	try {
		const post = await Post.findById(req.params.id);
		if (!post) {
			return res.status(404).json({
				message: `Unable to find the post with ID: ${req.params.id}`,
			});
		}
		if (!post.likes.includes(req.body.userId)) {
			await post.updateOne({ $push: { likes: req.body.userId } });
			res.status(200).json({ message: "The post has been liked." });
		} else {
			await post.updateOne({ $pull: { likes: req.body.userId } });
			res.status(200).json({ message: "The post has been disliked." });
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

const deletePost = async (req, res) => {
	try {
		let post;
		try {
			post = await Post.findById(req.params.id);
		} catch (err) {
			return res.status(404).json({
				message: `Unable to find the post with ID: ${req.params.id}`,
			});
		}
		if (post.userId === req.body.userId) {
			await Post.deleteOne();
			res.status(200).json({ message: "The post has been deleted." });
		} else {
			res.status(403).json({ message: "You can only delete your post." });
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

const getUserTimelinePost = async (req, res) => {
	try {
		const currentUser = await User.findById(req.body.userId);
		if (!currentUser)
			return res.status(404).json({ message: "User not found." });
		const userPosts = Post.find({ userId: currentUser._id });
		res.status(200).json(userPosts);
	} catch (err) {
		res.status(500).json(err);
	}
};

const getTimelinePosts = async (req, res) => {
	try {
		const currentUser = await User.findById(req.body.userId);
		if (!currentUser)
			return res.status(404).json({ message: "User not found." });
		const userPosts = await Post.find({ userId: currentUser._id });
		const friendPosts = await Promise.all(
			currentUser.followers.map((friendId) =>
				Post.find({ userId: friendId })
			)
		);
		res.status(200).json(userPosts.concat(friendPosts));
	} catch (err) {
		res.status(500).json(err);
	}
};

module.exports = {
	likePost,
	createPost,
	updatePost,
	deletePost,
	getPostById,
	getTimelinePosts,
	getUserTimelinePost,
};
