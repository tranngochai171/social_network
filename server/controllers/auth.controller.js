const User = require("../models/User");
const bcrypt = require("bcrypt");
const router = require("../routes/api/users.route");

// REGISTER
const register = async (req, res) => {
	try {
		const { username, email, pwd } = req.body;
		if (!username || !email || !pwd) {
			return res.status(400).json({
				message: "Username, Email and Password are required.",
			});
		}
		const salt = await bcrypt.genSalt(10);
		const hashPwd = await bcrypt.hash(pwd, salt);
		const user = await User.create({
			username,
			email,
			password: hashPwd,
		});
		const { password, ...userWithoutPassword } = user._doc;
		res.status(200).json(userWithoutPassword);
	} catch (err) {
		res.status(500).json(err);
	}
};

// LOGIN
const login = async (req, res) => {
	try {
		const { email, pwd } = req.body;
		if (!email || !pwd) {
			return res
				.status(400)
				.json({ message: "Email and Password are required." });
		}
		const user = await User.findOne({ email });
		if (!user) return res.status(404).json({ message: "User not found." });
		const match = await bcrypt.compare(pwd, user.password);
		if (!match) return res.status(404).json({ message: "Wrong password." });
		const { password, ...userWithoutPassword } = user._doc;
		res.status(200).json(userWithoutPassword);
	} catch (err) {
		res.status(500).json(err);
	}
};
module.exports = { register, login };
