class SocketService {
	connection(socket) {
		console.log(`User connect id is ${socket.id}`);
		__io.emit("firstEvent", "Hello this is a test");
		socket.on("disconnect", () => {
			console.log(`User disconnect id is ${socket.id}`);
		});
	}
}

module.exports = new SocketService();
