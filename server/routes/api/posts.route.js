const express = require("express");
const router = express.Router();
const postController = require("../../controllers/post.controller");

// Create a post
router.post("/", postController.createPost);
// Update a post
router.put("/:id", postController.updatePost);
// Delete a post
router.delete("/:id", postController.deletePost);
// Like a post
router.put("/:id/like", postController.likePost);
// Get a post
router.get("/:id", postController.getPostById);
// Get timeline posts
router.get("/timeline/all", postController.getTimelinePosts);
router.get("/timeline/user", postController.getUserTimelinePost);

module.exports = router;
